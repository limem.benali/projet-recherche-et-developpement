import numpy as np
from copy import copy

AireQLDA = 0.0
AireQLSBD = 0.0
AireBLDA = 0.0
Echeancier = []
QLDA = 0
QLSBD = 0
BLDA = 0
BLSBD = 0
NbCommandes = 0
listComm = []



class Produits:

    def __init__(self,idProduit,zonePrepa,stockProduit,idCommande,da,ds,traite):
        self.idProduit = idProduit
        self.zonePrepa = zonePrepa
        self.stockProduit = stockProduit

    def initListProduit(self):
        listProduit = []
        listProduit.append(Produits(1, 'LDA', 10000, None, None, None, False))
        listProduit.append(Produits(2, 'LDA', 10000, None, None, None, False))
        listProduit.append(Produits(3, 'LDA', 10000, None, None, None, False))
        listProduit.append(Produits(4, 'LDA', 10000, None, None, None, False))
        listProduit.append(Produits(5, 'LDA', 10000, None, None, None, False))
        listProduit.append(Produits(6, 'LDA', 10000, None, None, None, False))
        listProduit.append(Produits(7, 'LDA', 10000, None, None, None, False))
        return listProduit



class Commandes:

    def __init__(self,idCommande,listProduit,da,ds,listQteProduit):
        self.idCommande = idCommande
        self.listProduit = listProduit
        self.da = da
        self.listQteProduit = listQteProduit

    def initCommande(self,idCommande,listProduit,da):
        prod = []
        qteProdC = []
        i = np.random.randint(1, len(listProduit))
        for x in range(0,i-1):
            j = np.random.randint(0, len(listProduit)-1)
            tempQte = np.random.randint(1,30)
            if (listProduit[j].stockProduit - tempQte >= 0):
                prod.append(copy(listProduit[j]))
                qteProdC.append(tempQte)
                listProduit[j].stockProduit = listProduit[j].stockProduit - tempQte

        return Commandes(idCommande, prod, da, None, qteProdC)







