from model.ZonePrepa import ZonePrepa

'''
Classe de la zone palette complète 

'''
class ZonePalette(ZonePrepa):
    '''
     :param self: paramètre défaut
     :param idZonePrepa: identifiant de la zone de préparation
     :param dureeTraitement: durée nécessaire pour traiter un colis au sein de la zone de préparation
     :param nbRessources: Nombre de ressources alloué à la zone de préparation
     :param stockLocal: Tableau d'idenditifiants de produits ainsi que la quantité du stock du produit au sein de cette zone
     '''
    def __init__(self, idZonePrepa, dureeTraitement, nbRessources, stockLocal):
        super().__init__(idZonePrepa, dureeTraitement, nbRessources, stockLocal)
