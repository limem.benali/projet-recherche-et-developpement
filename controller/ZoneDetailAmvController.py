from controller.ProduitController import ProduitController
from model.Stock import Stock
from model.ZoneDetailAmv import ZoneDetailAmv
from model import utils

'''
Classe controller de la zone detail A moyenne vente 
'''
class ZoneDetailAmvController:
    '''
    :param self: paramètre défaut

    '''
    def __init__(self):
        pass
    '''
    :param self: paramètre défaut
    :return list_stock_produits: liste de produits stockés localement dans la zone de traitement 

    '''
    def initStockLocal(self):
        list_stock_produits = []

        list_produits = ProduitController.initProduits(ProduitController)

        for cptproduit in range(0,len(list_produits)):
            list_stock_produits.append(Stock(list_produits[cptproduit].idProduit, 300))

        return list_stock_produits

    def initZone(self):

        return ZoneDetailAmv(1, 40, 8, self.initStockLocal())

    def arriveeFileMV(self,idColis):
        global QMV
        global Echeancier
        global BMV
        global DateSimu

        #Incrémenter le nbCommandes dans la file LDA
        QMV = QMV + 1

        #Si LDA est libre
        if BMV < 3:
             #Insertion de l'evt Acces MV à DateSimu
            Echeancier.append([self.accesMV(idColis), DateSimu])
        #On trie
        Echeancier.sort(key=lambda x: x[1], reverse=False)

    def accesMV(self,idColis):
        global QMV
        global BMV
        global DateSimu
        global Echeancier
        global somme
        global listComm

        # On décrémente le nb de Commandes dans la file LDA
        QMV = QMV - 1

        # On occupe LDA
        BMV = BMV + 1

        # MàJ de la date grâce à la loi
        temp = listComm[NbCommandes - 1].listProduit
        max = 0
        for x in range(0, len(temp)):
            listComm[NbCommandes - 1].listProduit[x].da = listComm[NbCommandes - 1].da
            listComm[NbCommandes - 1].listProduit[x].ds = (
                        np.random.uniform(1 / 4, 13 / 12) + listComm[NbCommandes - 1].da)
            listComm[NbCommandes - 1].listProduit[x].traite = True

        for x in range(0, len(temp)):
            if (max < listComm[NbCommandes - 1].listProduit[x].ds):
                max = listComm[NbCommandes - 1].listProduit[x].ds
        newDate = max
        listComm[NbCommandes - 1].ds = max
        # print("Heure de départ", newDate)

        # Insertion dans l'échéancier
        Echeancier.append([DepartLDA, newDate])

        # On trie
        Echeancier.sort(key=lambda x: x[1], reverse=False)
