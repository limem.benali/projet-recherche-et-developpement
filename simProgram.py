from model.utils import *

DureeSimu = 20

def Debut():
    global DateSimu
    global Echeancier
    print(DateSimu)
    newDateCommande = np.random.exponential(0.5) + DateSimu

    Echeancier.append([ArriveeCommande, newDateCommande])
    Echeancier.append([Fin, DureeSimu])
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def MiseAJourDesAire(D1, D2):
    global AireQLDA
    global AireQLSBD
    global AireBLDA
    global QLDA
    global QLSBD
    global BLDA

    AireQLDA = AireQLDA + (D2 - D1) * QLDA
    AireQLSBD = AireQLSBD + (D2 - D1) * QLSBD
    AireBLDA = AireBLDA + (D2 - D1) * BLDA




def Simulateur():
    global DateSimu
    global Echeancier
    global QLDA
    global QLSBD
    global BLDA
    global BLSBD
    global NbCommandes
    global AireQLDA
    global AireQLSBD
    global AireBLDA

    QLSBD = 0
    BLDA = 0
    BLSBD = 0
    NbCommandes = 0
    AireQLDA = 0
    AireQLSBD = 0
    AireBLDA = 0

    DateSimu = 0
    Echeancier.append([Debut, DateSimu])
    while(Echeancier):
        #On récupère le premier couple
        firstPair = Echeancier[0]

        #On récupère la date
        Date = firstPair[1]

        #On met à jour les aires
        MiseAJourDesAire(DateSimu, Date)

        #On met à jour la date de simulation
        DateSimu = Date

        #On exécute l'evt Evt
        firstPair[0]()

        #On retire le couple (Evt, Date) utilisé
        if Echeancier:
            Echeancier.pop(0)


def ArriveeCommande():
    global NbCommandes
    global Echeancier
    global DateSimu
    global listComm

    #On calcule la date d'insertion
    newDateCommande = np.random.exponential(5) + DateSimu

    if(NbCommandes >= 1):
        listComm.append(Commandes.initCommande(Commandes, NbCommandes, listtest, newDateCommande))
    else:
        listComm.append(Commandes.initCommande(Commandes, NbCommandes, listtest, DateSimu))


    #On insère le couple l'evt ArriveCommande à DateSimu + e(1/2) (newDateCommande)
    Echeancier.append([ArriveeCommande, newDateCommande])


    #On incrémente le nb commande
    NbCommandes = NbCommandes + 1

    #On insère le couple l'evt ArriveeFileLDA à DateSimu
    Echeancier.append([ArriveeFileLDA, DateSimu])

    # Trier l'echancier par date afin que les evt soient stockés par ordre chronologique
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def ArriveeFileLDA():
    global QLDA
    global Echeancier
    global BLDA
    global DateSimu

    #Incrémenter le nbCommandes dans la file LDA
    QLDA = QLDA + 1

    #Si LDA est libre
    if BLDA < 3:
         #Insertion de l'evt AccessLDA à DateSimu
        Echeancier.append([AccesLDA, DateSimu])
    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def AccesLDA():
    global QLDA
    global BLDA
    global DateSimu
    global Echeancier
    global somme
    global listComm

    #On décrémente le nb de Commandes dans la file LDA
    QLDA = QLDA - 1

    #On occupe LDA
    BLDA = BLDA + 1

    #MàJ de la date grâce à la loi
    temp = listComm[NbCommandes-1].listProduit
    max = 0
    for x in range(0,len(temp)):
        listComm[NbCommandes - 1].listProduit[x].da = listComm[NbCommandes-1].da
        listComm[NbCommandes - 1].listProduit[x].ds = (np.random.uniform(1/4, 13/12) + listComm[NbCommandes-1].da)
        listComm[NbCommandes - 1].listProduit[x].traite = True

    for x in range(0, len(temp)):
        if (max < listComm[NbCommandes - 1].listProduit[x].ds ):
            max = listComm[NbCommandes - 1].listProduit[x].ds
    newDate = max
    listComm[NbCommandes-1].ds = max
    # print("Heure de départ", newDate)

    #Insertion dans l'échéancier
    Echeancier.append([DepartLDA, newDate])

    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)

def DepartLDA():
    global BLDA
    global Echeancier
    global DateSimu
    global QLDA

    #On libere le centre de controle
    BLDA = BLDA - 1

    #Si la file C est non vide
    if QLDA > 0 :
        #On ajoute l'evt AccesControle à DateSimu
        Echeancier.append([AccesLDA, DateSimu])


    #On trie
    Echeancier.sort(key=lambda x: x[1], reverse=False)


def Fin():
    global Echeancier
    global AireQLDA
    global AireQLSBD
    global AireBLDA
    global QLDA
    global QLSBD
    global BLDA
    global NbCommandes
    global listtest
    global listComm

    if(Echeancier):
        Echeancier.clear()
    if(NbCommandes > 0):
        tempsMoyFileLDA = AireQLDA / NbCommandes

    tailleMoyFileLDA = AireQLDA / DureeSimu


    print("Nb Commandes total", NbCommandes)
    for y in range(0,NbCommandes):
        print("Date de d'arrivée de la commande :", y+1,"DA:",listComm[y].da, "Date de finalisation des traitements : ",listComm[y].ds)
    for x in range(0,len(listtest)):
        print("Stock du produit : ", listtest[x].idProduit, "est de : ", listtest[x].stockProduit)




if __name__ == "__main__":
    listtest = Produits.initListProduit(Produits)
    Simulateur()






